.. SecKit For Splunk documentation master file, created by
   sphinx-quickstart on Tue Oct  9 17:51:28 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to SecKit For Splunk's documentation!
=============================================

The Success Enablement Content Kit (SecKit) project develops prescriptive guidance, and supporting add ons for the Splunk Eco system. Designed to help new implementations start well and existing implementations achive the next insight.

Sub Projects
------------

- :doc:`SecKit TA <ta:index>` Provide wrapper configuration and detailed guidances for add ons for Splunk
- :doc:`SecKit IDM <idm:index>` Provide modular dynamic asset and identities for Splunk Enterprise Security

.. toctree::
   :maxdepth: 2
   :caption: Contents:



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
